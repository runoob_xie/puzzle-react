### Hello Every
This is a verify component about vue,it's support vue2 and vue3,look next:

* 😀[puzzle-vue2](https://gitee.com/runoob_xie/puzzle)
* 🔥[puzzle-vue3](https://gitee.com/runoob_xie/puzzle-next)
- 😀[puzzle-react](https://gitee.com/runoob_xie/puzzle-react)

Hope to provide some help for you

- 

### 嗨 你好
这是一个基于vue的拼图验证组件,它目前提供了两个版本:
 
* 😀[puzzle-vue2](https://gitee.com/runoob_xie/puzzle)
* 🔥[puzzle-vue3](https://gitee.com/runoob_xie/puzzle-next)
- 😀[puzzle-react](https://gitee.com/runoob_xie/puzzle-react)

希望能给你提供一些帮助

### online example

-   😀[puzzle-vue2](https://codesandbox.io/s/puzzle-vue2-45w9b)
-   🔥[puzzle-vue3](https://codesandbox.io/s/puzzle-vue3-2glu2?file=/src/main.js)
-   🔥[puzzle-react](https://codesandbox.io/s/hopeful-keldysh-tut2w?file=/src/App.js)

### props 属性

|  name   | type  | default value |
|  ----  | ----   | ---- |
| width  | Number | 300 |
| height  | Number | 150 |
| offsetDistance  | Number | 10 |
| successText  | String | "校验成功" |
| failText  | String | "校验失败" |

### Emit event

```javascript
emit('complete',"success" || "fail")
```

### Connect 联系
 1. aursordev@gmail.com
 2. x17398977051@163.com


