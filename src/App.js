import React from "react";

import PuzzleNext from './components/puzzle-react';
import "./App.css";

function App() {
    return <div className="App">
        <PuzzleNext img="https://cdn.pixabay.com/photo/2020/08/09/11/31/business-5475283_960_720.jpg" />
    </div>;
}

export default App;
